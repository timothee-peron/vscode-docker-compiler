#!/bin/sh

k=0;
dockerTag=vscode-compiler

src=code;
pkg=VSCode-linux-x64;

help() {
  echo "Docked compiler script!";
  echo "Available options:";
  echo " --help -h       show this help";
  echo " -k              keep cached containers, otherwise will use docker build with --no-cache --rm=true";
  echo " -clean          don't build, clean built files";
  exit 0;
}

clean(){
  docker rmi $dockerTag;
  exit 0;
}


while [ $# -gt 0 ] ; do
  case "$1" in
    "-k")                 k=1;;
    "-clean")             clean;;
    "--help"|"-h")        help;;
    *)                    echo "Argument $1 is not accepted!"; exit 1;;
  esac
  shift;
done

if [ $k -eq 0 ]; then
  cacheParam="--no-cache --rm=true";
fi

echo running docker build command: docker build $cacheParam -t $dockerTag .;

docker build $cacheParam -t $dockerTag .;
docker run --rm -v "$(pwd)":/output $dockerTag cp /$pkg /output -r;
chmod o+rw ./$pkg -R;
