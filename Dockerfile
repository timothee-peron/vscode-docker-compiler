FROM debian:latest
# https://stackoverflow.com/a/57344191 :
SHELL ["/bin/bash", "--login", "-c"]

# Prerequisites : https://github.com/Microsoft/vscode/wiki/How-to-Contribute#build-and-run
RUN apt-get update \
&& apt-get install git python3 curl wget -y \
&& apt-get install build-essential g++ libx11-dev libxkbfile-dev libsecret-1-dev python-is-python3 fakeroot rpm -y

# dl and vscode libresprite

RUN cd / && git clone https://github.com/Microsoft/vscode.git

#nvm install
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash

RUN nvm install 16 \
&& nvm use 16 \
&& npm install -g yarn

COPY patch/ /patch/
RUN cp -rfv /patch/* /vscode

RUN cd /vscode \
&& yarn \ 
&& yarn gulp -LLLL vscode-linux-x64-min

