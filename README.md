# VScode Docker compiler

Execute dcompile.sh !

Some arguments could be provided:
  -  --help -h       show this help
  -  -k              keep cached containers, otherwise will use docker build with --no-cache --rm=true
  -  -clean          don't build, clean built files

Copy 'patch_sample' folder to 'patch' and edit images to have custom icons!

For gnome desktop integration, copy .desktop file to ~/.local/share/application (rename 'app-name' to match your executable). Suggested executable location is /opt.

## Marketplace extension

To add marketplace extensions, add to product.json (either in the patch folder of the source or in the resources/app folder of the built app):

https://github.com/Microsoft/vscode/issues/31168
```json
"extensionsGallery": {
    "serviceUrl": "https://marketplace.visualstudio.com/_apis/public/gallery",
    "cacheUrl": "https://vscode.blob.core.windows.net/gallery/index",
    "itemUrl": "https://marketplace.visualstudio.com/items"
}
```
